#!/usr/bin/env python

from aiohttp import web
from aiohttp_wsgi import WSGIHandler
from flask import Flask, render_template, abort, session, request, url_for, redirect, Markup
import core, config, os, hashlib, uuid, time, traceback, werkzeug, sys, subprocess, pickle, json, datetime
from urllib.parse import urlparse
from werkzeug.utils import secure_filename

def update_secret_key(app):
    app.secret_key = hashlib.sha512(str(uuid.uuid4()).encode('utf-8') + str(uuid.uuid4()).encode('utf-8')).hexdigest()
    
#Static redirects
def route_static_redirects(app):
    if not os.path.exists("static-redirects.txt"):
        return
    data = open("static-redirects.txt").read()
    data = data.replace("\r","").split("\n")
    for line in data:
        if not line:
            break
        a, b = tuple(line.split(" "))
        app.add_url_rule(a, b, lambda: werkzeug.utils.redirect(b))


#Initialize database
db = core.dbapi()
db.connect(config.db_host, config.db_port, config.db_name, config.db_user, config.db_pass)

#Flask app definition
app = Flask(__name__)

#Flask routes
@app.route("/v1/<action>/<key>", methods=["POST"])
def v1(action, key):
    return "FAIL", 500
@app.route("/v2/<action>", methods=["POST"])
def v2(action):
    return "FAIL", 500
######## **** API endpoints **** ########
# Version 3
#######  *********************** #######
@app.route("/v3/verify", methods=["POST"])
def remote_verify_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        return json.dumps((200, "OK"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))
@app.route("/v3/execute", methods=["POST"])
def remote_execute_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        query = request.form["query"]
        try:
            print("[API] /v3/execute: executing query: {}".format(query))
            return json.dumps((200, True))
        except:
            traceback.print_exc()
            return json.dumps((500, "Internal"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))
@app.route("/v3/select", methods=["POST"])
def remote_select_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        query = request.form["query"]
        try:
            print("[API] /v3/select: executing query: {}".format(query))
            ret = db.select(query)
            print("[API] /v3/select: result: {}".format(ret))
            final = []
            for res in ret:
                row = []
                for i, val in enumerate(res):
                    if type(val) == datetime.datetime:
                        row.append(int(time.mktime(val.timetuple())))
                    elif type(val) == uuid.UUID:
                        row.append(str(val))
                    else:
                        row.append(val)
                final.append(row)
            return json.dumps((200, final))
        except:
            traceback.print_exc()
            return json.dumps((500, "Internal"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))
@app.route("/v3/rm", methods=["POST"])
def remote_rm_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        public = request.form["public"]
        path = core.file_path(request.form["path"], public)
        try:
            print("[API] /v3/rm: removing file \"{}\"".format(path))
            os.remove(path)
            return json.dumps((200, "OK"))
        except:
            traceback.print_exc()
            return json.dumps((500, "Internal"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))

update_secret_key(app)
route_static_redirects(app)

#WSGI bridge 
wsgi = WSGIHandler(app)
aio = web.Application()

#AIOHTTP routes
aio.router.add_route("*", "/{path_info:.*}", wsgi)

print("[info] initialized successfully")

