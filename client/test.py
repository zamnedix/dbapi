#!/usr/bin/env python3

from remote_client import apiclient_v2, apiclient_v3
import sys, config 

print("--- API Version 2 ---")

c = apiclient_v2(config.api_server, config.api_key)
print(c.verify())
if not c.verify()[0]:
    print("Couldn't authenticate to API")
    sys.exit(1)
else:
    print("OK")

print("--- API Version 3 ---")
    
c = apiclient_v3(config.api_server, config.api_key)
print(c.verify())
print(c.list())
print(c.select("SELECT * FROM posts"))

